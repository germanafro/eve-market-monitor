import django.http
from django.shortcuts import render

from djangoProject.util.EveOAuth2 import EveOAuth2
from djangoProject.util.RestClient import BearerAuth, RestClient

sessions: dict = {}
oauth2 = EveOAuth2(
    "http://localhost:8000/sso/callback",
    "a4c3306f1c964919addd510956309d7b",
    "7GcUoCKYBiVmDFTLyMzAgsh1p1vrRndSpmINceS6"
)


def index(request):
    return render(request, 'homepage.html')


def callback(request):
    state = request.GET['state']
    code = request.GET['code']
    oauth2.acquire_token(code)
    token = oauth2.access_token
    return django.http.HttpResponse(
        RestClient().get(
            "https://esi.evetech.net/latest/markets/structures/1034280087100/",
            {
                "datasource": "tranquility",
                "page": "1"
            },
            BearerAuth(token)
        ).json()
    )
