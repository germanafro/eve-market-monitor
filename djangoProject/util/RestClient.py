import json
import webbrowser

import requests
from requests.auth import HTTPBasicAuth

PROTOCOL_HTTP = "http://"
PROTOCOL_HTTPS = "https://"
HEADER_AUTH = "Authorization"
REST_GET = "GET"
REST_POST = "POST"


def join_to_path(delimiter: str, args: list, prefix: str = ""):
    if args:
        return join_to_path(delimiter, args[1:], prefix + delimiter + str(args[0]))
    else:
        return prefix


def remove_suffix(input_string, suffix):
    if suffix and input_string.endswith(suffix):
        return input_string[:-len(suffix)]
    return input_string


def remove_prefix(input_string, prefix):
    if prefix and input_string.startswith(prefix):
        return input_string[len(prefix):]
    return input_string


def params_string_from_dict(params_map: dict):
    out_string = "?"
    for k, v in params_map.items():
        out_string = out_string + str(k) + "=" + str(v) + "&"
    return remove_suffix(out_string, "&")


class BearerAuth(requests.auth.AuthBase):
    def __init__(self, token):
        self.token = token

    def __call__(self, r):
        r.headers["authorization"] = "Bearer " + self.token
        return r


class RestClient:

    def __init__(self):
        self.delegate = requests

    def browser(self, href: str, params_map: dict = {}):
        address = href + params_string_from_dict(params_map)
        webbrowser.open(address)

    def get(self, href: str, params_map: dict = {}, auth=None):
        address = href + params_string_from_dict(params_map)
        print("connecting to: ", address)
        return self.delegate.get(address, auth=auth)

    def post(self, href: str, params_map: dict = {}, auth: HTTPBasicAuth = None, body: json = {},
             headers: dict = {'Content-type': 'application/json', 'Accept': 'application/json'}):
        address = href + params_string_from_dict(params_map)
        print("connecting to: ", address, body, headers)
        return self.delegate.post(address, auth=auth, json=body, headers=headers)
