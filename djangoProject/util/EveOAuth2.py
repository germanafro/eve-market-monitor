import uuid

from requests import Response
from requests.auth import HTTPBasicAuth
import webbrowser

from djangoProject.util.RestClient import RestClient

EVE_AUTHORIZE_BASE_URL = "https://login.eveonline.com/v2/oauth/authorize/"
EVE_TOKEN_BASE_URL = "https://login.eveonline.com/v2/oauth/token/"
PARAM_RESPONSE_TYPE = "response_type"
PARAM_REDIRECT_URI = "redirect_uri"
PARAM_CLIENT_ID = "client_id"
PARAM_SCOPE = "scope"
PARAM_STATE = "state"


class EveOAuth2:
    access_token: str
    refresh_token: str
    state: str
    handshake_response: Response
    token_response: Response
    client = RestClient()

    def __init__(
            self,
            redirect_uri: str,
            client_id: str,
            secret: str
    ):
        self.redirect_uri = redirect_uri
        self.client_id = client_id
        self.secret = secret

    def handshake(self, scopes_needed: str):
        self.state = str(uuid.uuid4())
        self.handshake_response = self.client.browser(
            EVE_AUTHORIZE_BASE_URL,
            {
                PARAM_RESPONSE_TYPE: "code",
                PARAM_REDIRECT_URI: self.redirect_uri,
                PARAM_CLIENT_ID: self.client_id,
                PARAM_SCOPE: scopes_needed,
                PARAM_STATE: self.state,
            }
        )

    def acquire_token(self, code: str):
        self.token_response = self.client.post(
            EVE_TOKEN_BASE_URL,
            {},
            HTTPBasicAuth(self.client_id, self.secret),
            {"grant_type": "authorization_code", "code": code}
        )
        print("token:", self.token_response.status_code, self.token_response.reason)
        if self.token_response.status_code == 200:
            self.access_token, self.refresh_token = self.tokens()

    def tokens(self):
        json_body = self.token_response.json()
        return json_body['access_token'], json_body['refresh_token']
